# Ancient
Gurney showed me this tablet he found on some far off secret island, but I have no idea what it says... (Standard flag format, letters in all caps)
### Walkthrough
- Simple translation of the Therapoda language used in the Dinotopia series. 
- Can be found by googling "Gurney dinosaur language" and following the wikipedia trail:
- Dinotopia -> google Dinotopian scripts found on that page -> Footprint Alphabet (translation image on wiki page)

